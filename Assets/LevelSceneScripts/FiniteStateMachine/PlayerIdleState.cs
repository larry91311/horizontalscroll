﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIdleState : PlayerBaseState
{
    
    public override void EnterSate(PlayerController_Ball player)
    {
        // player.SetExpression(player.idleSprite);

    }
    public override void Update(PlayerController_Ball player)
    {
        // if(Input.GetKeyDown(KeyCode.W))  //跳
        // {
        //     player.lastState=player.IdleState;
        //     player.TransitionToState(player.JumpState);
        // }
        

        // if(Input.GetKeyDown(KeyCode.A)||Input.GetKeyDown(KeyCode.D))    //移
        // {
        //     player.TransitionToState(player.MoveState);
        // }


        if (Input.GetKeyDown(KeyCode.Space))
		{
            player.lastState=player.IdleState;
			player.TransitionToState(player.AttackState);
		}


    }
    public override void OnCollisionEnter2D(PlayerController_Ball player)
    {
        player.OnAir=false;
        player.doubleJump=false;
    }
}
