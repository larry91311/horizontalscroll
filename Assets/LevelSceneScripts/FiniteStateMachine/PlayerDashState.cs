﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDashState : PlayerBaseState
{
    public override void EnterSate(PlayerController_Ball player)
    {
        // player.SetExpression(player.dashSprite);
    }
    public override void Update(PlayerController_Ball player)
    {
        if(Input.GetKey(KeyCode.D)||Input.GetKey(KeyCode.A))    //移
        {
            player.Rigidbody.velocity=new Vector2(Input.GetAxis("Horizontal")*20.0f,0);
        }
        else if(Input.GetKeyUp(KeyCode.D)||Input.GetKeyUp(KeyCode.A))
        {
            player.TransitionToState(player.MoveState);
        }
    }
    public override void OnCollisionEnter2D(PlayerController_Ball player)
    {
        
    }
}
