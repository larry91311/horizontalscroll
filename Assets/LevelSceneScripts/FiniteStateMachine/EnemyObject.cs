﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyObject : MonoBehaviour
{
    public int health = 100;
	public int EnemyExperience=90;

	

	
	
	private void Start() {
		
	}

	public void TakeDamage (int damage)
	{
		health -= damage;

		if (health <= 0)
		{
			ManagerGame.levelSystem.AddExperience(EnemyExperience);
			Die();
			
			
		}
	}

	void Die ()
	{
		
		Destroy(gameObject);
	}
}
