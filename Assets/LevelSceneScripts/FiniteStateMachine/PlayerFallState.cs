﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFallState : PlayerBaseState
{
    public override void EnterSate(PlayerController_Ball player)
    {
        
        // player.SetExpression(player.fallSprite);
        player.Rigidbody.velocity=new Vector2(0,player.fallForce);
        
    }
    public override void Update(PlayerController_Ball player)
    {
        
    }
    public override void OnCollisionEnter2D(PlayerController_Ball player)
    {   
        
        player.OnAir=false;
        player.doubleJump=false;
        player.TransitionToState(player.IdleState);
       
    }
}
