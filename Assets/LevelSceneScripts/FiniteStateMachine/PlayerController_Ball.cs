﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController_Ball : MonoBehaviour
{
    public Animator animator;


    public float jumpForce=0.0f;
    public float moveForce=0.0f;                //Force
    public float fallForce=0.0f;
    public float attckStateTimer;
    public float fallTimer;  //雙跳瞬間move掉下去左右變fall卡死
    public bool fallColli;

    

    private PlayerBaseState currentState;              //cu State
    public PlayerBaseState lastState;


    public Sprite idleSprite;
    public Sprite jumpSprite;
    public Sprite doubleJumpSprite;           //Sprites
    public Sprite dashSprite;
    public Sprite moveSprite;
    public Sprite fallSprite;
    public Sprite attackSprite;


    private SpriteRenderer stateSprite; 
                                                //取得SetExpression Sprite、取得OnColli2D Rigidbody2D
    private Rigidbody2D rbody;
    CircleCollider2D playerCollider;
    
    public bool OnAir=false;
    public bool doubleJump=false;           //雙重跳判斷跟ONAIR判斷
    private bool m_FacingRight = true;  // For determining which way the player is currently facing.




    public Transform firePoint;                 //射擊State所需
	public GameObject bulletPrefab;    




    bool isGrounded; // 紀錄是否在地上
	float groundCheckRadius = 0.2f; // 偵測地面範圍
    [Header("Jump")]
	[Space]
    public float jumpSpeed = 15f; // 跳躍向上力
	[SerializeField] Transform groundCheck; // 偵測地面的位置
	[SerializeField] LayerMask groundLayer; // 判斷哪些東西是地面


    Rigidbody2D playerRigidbody2D;// Player本身的Rigidbody2D
    Vector2 velocity = Vector2.zero;// 用於SmoothDamp
    [Header("Move")]
	[Space]
    [Range(0, .3f)] [SerializeField] float velocitySmoothTime = 0.05f;// 速度平滑時間


    public float moveSpeed = 400f;    //Playermovement.cs 功能
    float movement = 0f;



    bool jump = false;     //跳躍
    bool crouch = false;
    // Attack
    bool attack = false;
    //Skill
    bool skill=false;
    bool freeze = false;
    float preAttackTime = 0f, attackSpeed = 0.1f, bulletSpeed = 20f;

    // Crouch
	float ceilingCheckRadius = 0.2f;
	bool wasCrouch = false;
    [Header("Crouch")]
	[Space]
    [Range(0, 1)] [SerializeField] private float crouchSpeed = 0.36f; // 趴下後的速度
	[SerializeField] Transform ceilingCheck; // 偵測地面的位置


    [System.Serializable]
	public class BoolEvent : UnityEvent<bool> { }
	[Header("Event")]
	[Space]
	public BoolEvent onGroundChangedEvent;
	public BoolEvent onCrouchChangedEvent;
	public BoolEvent attackEvent;
    public BoolEvent skillEvent;

    public void OnGroundChange(bool isGrounded){
        animator.SetBool("Ground", isGrounded);
    }

     public void OnCrouchChange(bool crouch){
        animator.SetBool("Crouch", crouch);
        playerCollider.radius = crouch ? 0.15f : 0.32f;
    }

    






    private PlayerController_Ball player;      //載體

    private void Start() {
        playerRigidbody2D = GetComponent<Rigidbody2D>();
        playerCollider = GetComponent<CircleCollider2D>();
        
        OnAir=false;
        doubleJump=false;
        player=FindObjectOfType<PlayerController_Ball>();
        TransitionToState(player.IdleState);
    }





    public void TransitionToState(PlayerBaseState state)
    {
        currentState=state;
        currentState.EnterSate(this);
    }

    private void OnCollisionEnter2D(Collision2D other) {
        currentState.OnCollisionEnter2D(this);
    }

    private void Update() {
        // 水平移動
        movement = Input.GetAxisRaw("Horizontal") * moveSpeed;
        animator.SetFloat("Speed", Mathf.Abs(movement));
        // 跳躍
        jump = Input.GetButtonDown("Jump");
        //趴下
        crouch = Input.GetButton("Crouch");
        // 攻擊
        attack = Input.GetKey(KeyCode.Space);
        if(!attack){
            animator.SetBool("Attack", false);
        }
        skill = Input.GetKeyDown(KeyCode.S);


        currentState.Update(this);    

        


        

        
    }



    private void FixedUpdate()
	{
        if(freeze) return;
        // 更新Player狀態
        
        Move(movement*Time.fixedDeltaTime, jump, crouch, attack, skill);

        CheckGrounded(); //檢查地面

        
        if (movement > 0 && !m_FacingRight)      //轉向
        {
            // ... flip the player.
            Flip();
        }
        // Otherwise if the input is moving the player left and the player is facing right...
        else if (movement < 0 && m_FacingRight)
        {
            // ... flip the player.
            Flip();
        }



        // if (Input.GetKey(KeyCode.Space))
		// {
        //     player.lastState=player.IdleState;
		// 	player.TransitionToState(player.playerContinueShooting);
		// }


        




		
	}


    public PlayerBaseState CurrentState
    {
        get
        {
            return currentState;
        }
    }

    public Rigidbody2D Rigidbody
    {
        get
        {
            return playerRigidbody2D;
        }
    }


    public readonly PlayerIdleState IdleState=new PlayerIdleState();
    public readonly PlayerJumpState JumpState=new PlayerJumpState();
    public readonly PlayerDoubleJumpState DoubleJumpState=new PlayerDoubleJumpState();
    public readonly PlayerFallState FallState=new PlayerFallState();
    public readonly PlayerAttackState AttackState=new PlayerAttackState();
    public readonly PlayerDashState DashState=new PlayerDashState();
    public readonly PlayerMoveState MoveState=new PlayerMoveState();
    public readonly PlayerContinueShootingState playerContinueShooting=new PlayerContinueShootingState();
    

    private void Awake()
    {
        stateSprite = GetComponentInChildren<SpriteRenderer>();
        
        SetExpression(idleSprite);
    }

    




    public void SetExpression(Sprite newExpression)
    {
        stateSprite.sprite = newExpression;
    }









    private void Flip()
	{
		// Switch the way the player is labelled as facing.
		m_FacingRight = !m_FacingRight;

		transform.Rotate(0f, 180f, 0f);
	}

    public void Shoot()
    {
        Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
    }


    public void Attack(bool facingRight){
        animator.SetBool("Attack", true);
        if(Time.time - preAttackTime >= attackSpeed){
            GameObject go = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
            go.GetComponent<Rigidbody2D>().velocity = Vector2.right * bulletSpeed * (m_FacingRight ? 1 : -1);
            preAttackTime = Time.time;
        }
    }


    public void Move(float movement, bool jump, bool crouch, bool attack, bool skill){
		// Crouch
		if(!crouch & wasCrouch){
			// 當放開趴下鍵時需要偵測是否能站起
			crouch = Physics2D.OverlapCircle(ceilingCheck.position, ceilingCheckRadius, groundLayer) != null;
		}
		crouch &= isGrounded; // 必須在地板才能趴下
		if(crouch){
			movement *= crouchSpeed;
		}
		if(crouch != wasCrouch){
			onCrouchChangedEvent.Invoke(crouch);
		}
		wasCrouch = crouch;
		
		// Jump
		if(jump && isGrounded){
			Vector2 v = playerRigidbody2D.velocity;
			v.y = jumpSpeed;
			playerRigidbody2D.velocity = v;
		}

		// Move
		// 先計算目標速度
		Vector2 targetVelocity = new Vector2(movement, playerRigidbody2D.velocity.y);
		// 平滑計算
		playerRigidbody2D.velocity = Vector2.SmoothDamp(playerRigidbody2D.velocity, targetVelocity, ref velocity, velocitySmoothTime);

		// Flip
		if(movement > 0 && !m_FacingRight){
			Flip();
		}
		else if(movement < 0 && m_FacingRight){
			Flip();
		}

		// Attack
		if(!crouch && attack){ // 當非趴下狀態時才可以攻擊
			attackEvent.Invoke(m_FacingRight);
		}
		// Skill
		if(skill){
			skillEvent.Invoke(m_FacingRight);
		}
	}


    void CheckGrounded(){
        bool wasGrounded = isGrounded;
		// 判斷偵測範圍內是否有地面
		isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, groundLayer) != null;
        if(wasGrounded != isGrounded){
			onGroundChangedEvent.Invoke(isGrounded);
		}
	}
    

    public void Skill(bool facingRight){
        freeze = true;
        animator.SetBool("Skill", true);
        transform.DOMove(transform.position + Vector3.right * 3 * (facingRight ? 1 : -1), 0.1f).OnComplete(SkillEnd);
    }


    void SkillEnd(){
        skill = false;
        animator.SetBool("Skill", false);
        freeze = false;
    }

}
