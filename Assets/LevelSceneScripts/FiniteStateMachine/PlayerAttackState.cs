﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackState : PlayerBaseState
{
    public override void EnterSate(PlayerController_Ball player)
    {
        
        
        player.Shoot();
    }
    public override void Update(PlayerController_Ball player)
    {
        
        player.attckStateTimer+=Time.deltaTime;
        if(Input.GetKeyUp(KeyCode.Space))
        {
            
            player.TransitionToState(player.lastState);
        }
        player.lastState=player.IdleState;
    }
    public override void OnCollisionEnter2D(PlayerController_Ball player)
    {

    }
}
