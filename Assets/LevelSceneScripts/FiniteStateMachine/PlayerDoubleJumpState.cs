﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDoubleJumpState : PlayerBaseState
{
    public override void EnterSate(PlayerController_Ball player)
    {
        // player.SetExpression(player.doubleJumpSprite);
        player.Rigidbody.velocity=new Vector2(0,player.jumpForce*0.5f);
        player.doubleJump=true;
    }
    public override void Update(PlayerController_Ball player)
    {
        // if(Input.GetKeyDown(KeyCode.S)&&player.OnAir==true)
        // {
        //     player.TransitionToState(player.FallState);      //下落
        // }
        // if(Input.GetKeyDown(KeyCode.A)||Input.GetKeyDown(KeyCode.D)&&player.OnAir==true)    //移
        // {
        //     player.TransitionToState(player.MoveState);
        // }
    }
    public override void OnCollisionEnter2D(PlayerController_Ball player)
    {
        
        player.OnAir=false;
        player.doubleJump=false;
        player.TransitionToState(player.IdleState);
    }
}
