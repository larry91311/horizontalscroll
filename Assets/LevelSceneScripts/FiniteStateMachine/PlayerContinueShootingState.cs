﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerContinueShootingState : PlayerBaseState
{
    public override void EnterSate(PlayerController_Ball player)
    {
        
        
        
    }
    public override void Update(PlayerController_Ball player)
    {
        player.Shoot();
        if(Input.GetKeyDown(KeyCode.S))
        {
            player.TransitionToState(player.FallState);      //下落
        }
        if(Input.GetKeyUp(KeyCode.Space))
        {
            player.TransitionToState(player.lastState);
        }
    }
    public override void OnCollisionEnter2D(PlayerController_Ball player)
    {

    }
}
