﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveState : PlayerBaseState
{
    public override void EnterSate(PlayerController_Ball player)
    {
        // player.SetExpression(player.moveSprite);

        
        
        
        
    }
    public override void Update(PlayerController_Ball player)
    {
        
        if(Input.GetKey(KeyCode.D)||Input.GetKey(KeyCode.A))    //移
        {
            if(Input.GetAxis("Horizontal")<0.9f||Input.GetAxis("Horizontal")>-0.9f)
            {
                player.Rigidbody.velocity=new Vector2(Input.GetAxis("Horizontal")*10.0f,0);
            }
            else
            {
                player.Rigidbody.velocity=new Vector2(Input.GetAxis("Horizontal")*50.0f,0);
            }
            
        }
        else if((Input.GetKeyUp(KeyCode.A)||Input.GetKeyUp(KeyCode.D))&&player.doubleJump==true&&player.OnAir==true)  //有雙跳下落
        {
            player.TransitionToState(player.FallState);
        }

        if(Input.GetKeyUp(KeyCode.A)||Input.GetKeyUp(KeyCode.D)&&player.OnAir==false&&player.doubleJump==false)     //移動鍵放開IDLE
        {
            player.TransitionToState(player.IdleState);
        }
        else if((Input.GetKeyUp(KeyCode.A)||Input.GetKeyUp(KeyCode.D))&&player.OnAir==true&&player.doubleJump==false) //沒雙跳可回到跳
        {
            player.TransitionToState(player.JumpState);
        }
        else if((Input.GetKeyUp(KeyCode.A)||Input.GetKeyUp(KeyCode.D))&&player.OnAir==true&&player.doubleJump==true)  //有雙跳下落
        {
            player.TransitionToState(player.FallState);
        }


        if (Input.GetKeyDown(KeyCode.Space))
		{
            player.lastState=player.MoveState;
			player.TransitionToState(player.AttackState);
		}
        
    }
    public override void OnCollisionEnter2D(PlayerController_Ball player)
    {
        player.OnAir=false;
        player.doubleJump=false;
    }
}
