﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerGame : MonoBehaviour
{
    [SerializeField]
    private LevelWindowMy levelWindow;

    [SerializeField]
    private PlayerMy player;
    

    public static LevelSystemMy levelSystem;
    
    private void Awake()
    {
        levelSystem=new LevelSystemMy();
        
        

        
        
        
    }


    // Start is called before the first frame update
    void Start()
    {
        levelWindow.SetLevelSystem(levelSystem);
        player.SetLevelSystem(levelSystem);

        LevelSystemAnimatedMy levelSystemAnimated=new LevelSystemAnimatedMy(levelSystem);
        levelWindow.SetLevelSystemAnimated(levelSystemAnimated);
    }

    // Update is called once per frame
    void Update()
    {
        // levelWindow.SetLevelSystem(levelSystem);
        // levelWindow.SetLevelSystemAnimated(levelSystemAnimated);
        // player.SetLevelSystemAnimated(levelSystemAnimated);
    }
}
