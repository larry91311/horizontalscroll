﻿using UnityEngine;

public abstract class PlayerBaseState
{
    public abstract void EnterSate(PlayerController_Ball player);
    public abstract void Update(PlayerController_Ball player);
    public abstract void OnCollisionEnter2D(PlayerController_Ball player);

}
