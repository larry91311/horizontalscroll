﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJumpState : PlayerBaseState
{
    public override void EnterSate(PlayerController_Ball player)
    {
        player.OnAir=true;
        // player.SetExpression(player.jumpSprite);

        Vector2 v = player.Rigidbody.velocity;      //velocity.y=jumpSpeed
        v.y = player.jumpSpeed;
        player.Rigidbody.velocity = v;
        
    }
    public override void Update(PlayerController_Ball player)
    {
        if(Input.GetKeyDown(KeyCode.W)&&player.OnAir==true&&player.doubleJump==false)
        {
            player.TransitionToState(player.DoubleJumpState);      //雙跳
        }
        if(Input.GetKeyDown(KeyCode.S)&&player.OnAir==true)
        {
            player.TransitionToState(player.FallState);      //下落
        }
        // if(Input.GetKeyDown(KeyCode.A)||Input.GetKeyDown(KeyCode.D))    //移
        // {
        //     player.TransitionToState(player.MoveState);
        // }

        // if (Input.GetKeyDown(KeyCode.Space))
		// {
        //     player.lastState=player.JumpState;
		// 	player.TransitionToState(player.AttackState);
		// }
        



    }
    public override void OnCollisionEnter2D(PlayerController_Ball player)
    {
        player.doubleJump=false;
        player.OnAir=false;
        player.TransitionToState(player.IdleState);
        
    }
}

