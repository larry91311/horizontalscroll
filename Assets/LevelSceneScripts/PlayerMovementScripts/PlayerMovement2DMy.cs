﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(CircleCollider2D))]
public class PlayerMovement2DMy : MonoBehaviour
{
    public LayerMask groundLayer;
    public CharacterController2DMy controller2D;
    public Animator animator;
    CircleCollider2D playerCollider;
    // Move
    public float moveSpeed = 400f;
    float movement = 0f;
    // Jump
    bool jump = false;
    // Crouch
    bool crouch = false;
    // Attack
    bool attack = false;
    // Skill
    bool skill = false;
    bool freeze = false;
    bool skillCircleShoot=false;
    public GameObject bullet;
    float preAttackTime = 0f, attackSpeed = 0.1f, bulletSpeed = 20f;
    private float nextFlyTimer=0f;

    [Header("WasAttack")]
    private int playerHealth=300;


    private GameObject bulletPrefab;


    [Header("Skill")]
	[Space]
    private bool doodle=false;
    private bool flyState=false;
    private bool laser=false;

    private bool dash=true;




    [Header("LaserPlace")]
	[Space]
    public int damage = 40;
    public LineRenderer lineRenderer;
    
    public Vector3 laserShootPosOffSet;

    private void Awake() {
        playerCollider = GetComponent<CircleCollider2D>();
    }

    // 在Update中進行輸入偵測
    void Update()
    {
        // 水平移動
        movement = Input.GetAxisRaw("Horizontal") * moveSpeed;
        animator.SetFloat("Speed", Mathf.Abs(movement));
        // 跳躍
        jump = Input.GetButtonDown("Jump");
        
        // 趴下
        crouch = Input.GetButton("Crouch");
        // 攻擊
        attack = Input.GetKey(KeyCode.Space);
        if(!attack){
            animator.SetBool("Attack", false);
        }
        
        skill = Input.GetKeyDown(KeyCode.S);


        if(Input.GetKeyDown(KeyCode.G))    //CircleShoot Skill G 觸發
        {
            AllSetFalse();


            skillCircleShoot=true;
        }
        
        if(Input.GetKeyDown(KeyCode.F))   //Fly Skill F 觸發
        {
            AllSetFalse();


            flyState=true;
        }
        
        if(Input.GetKeyDown(KeyCode.H))   //Dash Skill H 觸發
        {
            AllSetFalse();


            
        }

        if(Input.GetKeyDown(KeyCode.J))   //Dash Skill H 觸發
        {
            AllSetFalse();

            laser=true;

            
        }

        
        
        


       
    }


    // 在FixedUpdate中改變物理狀態
    private void FixedUpdate() {
        // 更新Player狀態
        controller2D.Move(movement*Time.fixedDeltaTime, jump, crouch, attack, skill,skillCircleShoot,flyState,laser);
    }

    private void AllSetFalse()
    {
        skillCircleShoot=false;
        flyState=false;
        laser=false;
    }

    public void OnGroundChange(bool isGrounded){
        animator.SetBool("Ground", isGrounded);
    }

    public void OnCrouchChange(bool crouch){
        animator.SetBool("Crouch", crouch);
        playerCollider.radius = crouch ? 0.15f : 0.32f;
    }

    public void Attack(bool facingRight){
        animator.SetBool("Attack", true);
        if(Time.time - preAttackTime >= attackSpeed){
            GameObject go = Instantiate(bullet, transform.position, Quaternion.identity);
            go.GetComponent<Rigidbody2D>().velocity = Vector2.right * bulletSpeed * (facingRight ? 1 : -1);
            preAttackTime = Time.time;
        }
    }
    // public void ManySkill(bool facingRight,bool circleShoot)
    // {
    //     if(circleShoot==false)   //Dash
    //     {
    //         freeze = true;
    //         animator.SetBool("Skill", true);
    //         transform.DOMove(transform.position + Vector3.right * 3 * (facingRight ? 1 : -1), 0.1f).OnComplete(SkillEnd);
    //     }

    //     if(circleShoot==true)    //circleShoot
    //     {
    //         animator.SetBool("Skill", true);
    //         StartCoroutine(Shooting());
    //         animator.SetBool("Skill", false);
    //         skillCircleShoot=false;
    //     }
    // }

    public void Skill(bool facingRight){
        
        
        freeze = true;
        animator.SetBool("Skill", true);
        
        RaycastHit2D hitInfo=Physics2D.Raycast(transform.position,Vector3.right*(facingRight ? 1 : -1),3.0f,groundLayer);

        
        
        if(hitInfo.collider==null)
        {
            transform.DOMove(transform.position + Vector3.right * 3 * (facingRight ? 1 : -1), 0.1f).OnComplete(SkillEnd);
        }
        else
        {
            SkillEnd();
        }
        
        

        // if(circleShoot==true)
        // {
        //     animator.SetBool("Skill", true);
        //     StartCoroutine(Shooting());
        //     animator.SetBool("Skill", false);
        // }

        


        
    }

    void SkillEnd(){
        skill = false;
        animator.SetBool("Skill", false);
        freeze = false;
    }


    public void CircleShoot(bool CircleShoot)
    {
        animator.SetBool("Skill", true);
        StartCoroutine(Shooting());
        animator.SetBool("Skill", false);

        
    }

    public void Fly(bool fly)
    {
        
        freeze = true;
        animator.SetBool("Skill", true);
        if(Time.time>nextFlyTimer)
        {
            transform.DOMove(transform.position + Vector3.up * 3 , 0.1f).OnComplete(SkillEnd);
            float firerate=0.3f;
            nextFlyTimer=Time.time+firerate;
        }
        else
        {
            SkillEnd();
        }
        
        animator.SetBool("Skill", false);
    }
    public Transform laserPoint;

    public void RaycastLaser(bool laser)
    {
        freeze = true;
        animator.SetBool("Skill", true);

        StartCoroutine(LaserShoot());

        SkillEnd();
        animator.SetBool("Skill", false);
    }

    IEnumerator LaserShoot()
    {
        
        
        while(Input.GetKey(KeyCode.S))
        {
        float Direction=controller2D.facingRight?1:-1;
        laserPoint.position=transform.position+laserShootPosOffSet*Direction;

        
        RaycastHit2D hitInfo = Physics2D.Raycast(laserPoint.position,Direction*transform.right);
        if (hitInfo)
        {
        
            lineRenderer.SetPosition(0, laserPoint.position);
            lineRenderer.SetPosition(1, hitInfo.point);
        }
        else
        {
            lineRenderer.SetPosition(0, laserPoint.position);
			lineRenderer.SetPosition(1, laserPoint.position + Direction*laserPoint.right * 1000);
        }

        lineRenderer.enabled = true;

        yield return null;
        }

		lineRenderer.enabled = false;

    }

    private void OnTriggerEnter2D(Collider2D other) {
        
            
    }



    IEnumerator Shooting()
    {
        int n=12;
        float angle=360;
        for(int i=0;i<n;i++)
        {
            
            GameObject go = Instantiate(bullet, transform.position,Quaternion.identity);
            Rigidbody2D goRig=go.GetComponent<Rigidbody2D>();
            goRig.velocity=new Vector2(Mathf.Cos(angle/180*Mathf.PI),Mathf.Sin(angle/180*Mathf.PI))*bulletSpeed;
            angle-=360/n*2;
            
            yield return null;
        }
        
    }

    

    

    
}