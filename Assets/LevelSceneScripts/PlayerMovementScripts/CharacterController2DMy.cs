﻿using UnityEngine;
using UnityEngine.Events;
using CodeMonkey;
using CodeMonkey.Utils;

[RequireComponent(typeof(Rigidbody2D))]
public class CharacterController2DMy : MonoBehaviour
{
	public Transform laserShootPos;
	public MeshParticleSystemMy meshParticleSystem; 
	public Transform shootPosTransform;
	public Transform dropBulletTransform;
	Rigidbody2D playerRigidbody2D; // Player本身的Rigidbody2D
	// Move
	Vector2 velocity = Vector2.zero; // 用於SmoothDamp
	[Header("Move")]
	[Space]
    [Range(0, .3f)] [SerializeField] float velocitySmoothTime = 0.05f; // 速度平滑時間
	// Jump
	public bool isGrounded; // 紀錄是否在地上
	float groundCheckRadius = 0.2f; // 偵測地面範圍
	[Header("Jump")]
	[Space]
    [SerializeField] float jumpSpeed = 15f; // 跳躍向上力
	[SerializeField] Transform groundCheck; // 偵測地面的位置
	[SerializeField] LayerMask groundLayer; // 判斷哪些東西是地面
	// Crouch
	float ceilingCheckRadius = 0.2f;
	bool wasCrouch = false;
    [Header("Crouch")]
	[Space]
    [Range(0, 1)] [SerializeField] private float crouchSpeed = 0.36f; // 趴下後的速度
	[SerializeField] Transform ceilingCheck; // 偵測地面的位置
	// Attack
	public bool facingRight = true;
	// Event
	[System.Serializable]
	public class BoolEvent : UnityEvent<bool> { }
	[Header("Event")]
	[Space]
	public BoolEvent onGroundChangedEvent;
	public BoolEvent onCrouchChangedEvent;
	public BoolEvent attackEvent;
	public BoolEvent skillEvent;






	


	

	private void Awake() {
		playerRigidbody2D = GetComponent<Rigidbody2D>();
	}

	private void FixedUpdate() {
		CheckGrounded();
		
	}

	// 偵測是否在地面
	void CheckGrounded(){
		// 判斷偵測範圍內是否有地面
		bool wasGrounded = isGrounded;
		isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, groundLayer) != null;
		
		if(wasGrounded != isGrounded){
			onGroundChangedEvent.Invoke(isGrounded);
		}
	}

	public void Move(float movement, bool jump, bool crouch, bool attack,bool skill,bool circleShoot,bool fly,bool laser){
		// Crouch
		if(!crouch & wasCrouch){
			// 當放開趴下鍵時需要偵測是否能站起
			crouch = Physics2D.OverlapCircle(ceilingCheck.position, ceilingCheckRadius, groundLayer) != null;
		}
		crouch &= isGrounded; // 必須在地板才能趴下
		if(crouch){
			movement *= crouchSpeed;
		}
		if(crouch != wasCrouch){
			onCrouchChangedEvent.Invoke(crouch);
		}
		wasCrouch = crouch;
		
		// Jump
		if(jump && isGrounded){
			Vector2 v = playerRigidbody2D.velocity;
			v.y = jumpSpeed;
			playerRigidbody2D.velocity = v;
		}

		// Move
		// 先計算目標速度
		Vector2 targetVelocity = new Vector2(movement, playerRigidbody2D.velocity.y);
		// 平滑計算
		playerRigidbody2D.velocity = Vector2.SmoothDamp(playerRigidbody2D.velocity, targetVelocity, ref velocity, velocitySmoothTime);

		// Flip
		if(movement > 0 && !facingRight){
			Flip();
		}
		else if(movement < 0 && facingRight){
			Flip();
		}

		// Attack
		if(!crouch && attack){ // 當非趴下狀態時才可以攻擊
			
			attackEvent.Invoke(facingRight);

			

			

			//On_Shoot_MeshParticleSystem();
			On_Shoot_ShellParticleSystemHandlerMy();

		}

		// Skill
		if(skill&&circleShoot==false&&fly==false&&laser==false){
			AllSkillEventSetOFF();
			skillEvent.SetPersistentListenerState(0,UnityEventCallState.EditorAndRuntime); //設Event狀態
			

			skillEvent.Invoke(facingRight);

		}
		else if(skill&&circleShoot==true&&fly==false&&laser==false)
		{
			AllSkillEventSetOFF();
			skillEvent.SetPersistentListenerState(1,UnityEventCallState.EditorAndRuntime);
			

			skillEvent.Invoke(circleShoot);
			
		}
		else if(skill&&circleShoot==false&&fly==true&&laser==false)
		{
			AllSkillEventSetOFF();
			skillEvent.SetPersistentListenerState(2,UnityEventCallState.EditorAndRuntime);

			skillEvent.Invoke(fly);
			
		}
		else if(skill&&circleShoot==false&&fly==false&&laser==true){
			AllSkillEventSetOFF();
			skillEvent.SetPersistentListenerState(3,UnityEventCallState.EditorAndRuntime);

			skillEvent.Invoke(laser);

		}
		
		
		
	}

	private void AllSkillEventSetOFF()
	{
		int skillEventNum=skillEvent.GetPersistentEventCount();
		for(int i=0;i<skillEventNum;i++)
		{
			skillEvent.SetPersistentListenerState(i,UnityEventCallState.Off);
		}
	}

	void Flip(){
		// 改變朝向
		facingRight = ! facingRight;
		// 改變圖片方向
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;


		
	}

	void On_Shoot_MeshParticleSystem()
	{
		Vector3 quadPosition=shootPosTransform.position;
			Vector3 quadSize=new Vector3(0.1f,0.01f); //長方形
			float rotation=0f;

			int uvIndex = Random.Range(0, 8);
			// meshParticleSystem.AddQuad(shootPosTransform.position);
			int spawnedQuadIndex=meshParticleSystem.AddQuad(shootPosTransform.position,rotation,quadSize,true,uvIndex);
			

			FunctionUpdater.Create(() => 
			{
				quadPosition += new Vector3(0.1f, 0.1f) * 3f * Time.deltaTime;
				quadSize +=  new Vector3(0.1f,0.05f)*3f * Time.deltaTime;     //長方形的增加，如果加正方形比例增幅會蓋過初始變正方形
                rotation += 360f * Time.deltaTime;

				meshParticleSystem.UpdateQuad(spawnedQuadIndex, quadPosition, rotation, quadSize,true,uvIndex);
			});
	}

	void On_Shoot_ShellParticleSystemHandlerMy()
	{
		Vector3 quadPosition = shootPosTransform.position;

		Vector3 shootDir = (shootPosTransform.position - playerRigidbody2D.transform.position).normalized;
		quadPosition += (shootDir * -1f) * 8f;
		
		Vector3 theScale = transform.localScale; //目前scale方向

		float applyRotation =theScale.x*Random.Range(-95f, -85f);

		Vector3 shellMoveDir = UtilsClass.ApplyRotationToVector(shootDir, applyRotation);

		ShellParticleSystemHandlerMy.Instance.SpawnShell(dropBulletTransform.position,shellMoveDir);
	}



}