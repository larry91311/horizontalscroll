﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMy : MonoBehaviour
{
    public int damage = 40;
    public float speed = 20f;
    public float AttackDistance = 20;
    float startPosition;
    public Rigidbody2D rb;
    private void Start() {
        

        startPosition = transform.position.x;
    }

    private void FixedUpdate() {
        if(Mathf.Abs(transform.position.x - startPosition) > AttackDistance){
            Destroy(gameObject);
        }
    }
    
    private void OnTriggerEnter2D(Collider2D other) {
        if(other.tag=="Boss")
        {
            Boss bossScript=other.GetComponent<Boss>();
            bossScript.TakeDamage(damage);
            Destroy(gameObject);
        }
        switch(LayerMask.LayerToName(other.transform.gameObject.layer)){
            case "Ground":
                Destroy(gameObject);
                break;
        }
    }
}
