﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey;
using CodeMonkey.Utils;
public class MeshParticleSystemMy : MonoBehaviour
{
    // Set in the Editor using Pixel Values
    [System.Serializable]
    public struct ParticleUVPixels
    {
        public Vector2Int uv00Pixels;
        public Vector2Int uv11Pixels;
    }

    // Holds normalized texture UV Coordinates,convert pixel to normalized value
    private struct UVCoords
    {
        public Vector2 uv00;
        public Vector2 uv11;
    }



    [SerializeField] private ParticleUVPixels[] particleUVPixelsArray;
    private UVCoords[] uvCoordsArray;

    private int quadIndex;   //10
    private const int MAX_QUAD_AMOUNT = 15000;


    private Mesh mesh;
    private Vector3[] vertices;
    private Vector2[] uv;
    private int[] triangles;

    private void Awake() {
        mesh=new Mesh();


        vertices=new Vector3[4*MAX_QUAD_AMOUNT];
        uv=new Vector2[4*MAX_QUAD_AMOUNT];
        triangles=new int[6*MAX_QUAD_AMOUNT];

        // AddQuad(new Vector3(-3,0));  //9
        // AddQuad(new Vector3(4,0));

        mesh.vertices=vertices;
        mesh.uv=uv;
        mesh.triangles=triangles;

        GetComponent<MeshFilter>().mesh=mesh;

        Material material=GetComponent<MeshRenderer>().material;
        Texture mainTexture=material.mainTexture;
        int textureWidth=mainTexture.width;
        int textureHeight=mainTexture.height;

        List<UVCoords> uvCoordsList=new List<UVCoords>();
        foreach(ParticleUVPixels particleUVPixels in particleUVPixelsArray)
        {
            UVCoords uVCoords=new UVCoords
            {
                uv00 = new Vector2((float)particleUVPixels.uv00Pixels.x / textureWidth, (float)particleUVPixels.uv00Pixels.y / textureHeight),
                uv11 = new Vector2((float)particleUVPixels.uv11Pixels.x / textureWidth, (float)particleUVPixels.uv11Pixels.y / textureHeight),
            };

            uvCoordsList.Add(uVCoords);
        }
        uvCoordsArray=uvCoordsList.ToArray();
   }

   public int AddQuad(Vector3 position,float rotation,Vector3 quadSize,bool skewed,int uvIndex)        //6 pos
    {
        if (quadIndex >= MAX_QUAD_AMOUNT) return 0; // Mesh full   1   
        
        UpdateQuad(quadIndex, position, 0f, new Vector3(0.1f,0.1f),true,uvIndex);

        int spawnedQuadIndex = quadIndex;

        quadIndex++;

        return spawnedQuadIndex;


        


    }


    public void UpdateQuad(int quadIndex, Vector3 position, float rotation, Vector3 quadSize,bool skewed,int uvIndex)
    {
        //Relocate vertices
        int vIndex = quadIndex * 4;
        int vIndex0 = vIndex;
        int vIndex1 = vIndex + 1;
        int vIndex2 = vIndex + 2;
        int vIndex3 = vIndex + 3;

        // // 原UV
        // uv[vIndex0] = new Vector2(0,0);
        // uv[vIndex1] = new Vector2(0,1);
        // uv[vIndex2] = new Vector2(1,1);
        // uv[vIndex3] = new Vector2(1,0);

        // UV
        UVCoords uvCoords = uvCoordsArray[uvIndex];
        uv[vIndex0] = uvCoords.uv00;
        uv[vIndex1] = new Vector2(uvCoords.uv00.x, uvCoords.uv11.y);
        uv[vIndex2] = uvCoords.uv11;
        uv[vIndex3] = new Vector2(uvCoords.uv11.x, uvCoords.uv00.y);

        // Vector3 quadSize=new Vector3(0.1f,0.1f);   //7
        // float rotation=0f;
        if (skewed) {
            vertices[vIndex0] = position + Quaternion.Euler(0, 0, rotation) * new Vector3(-quadSize.x, -quadSize.y);
            vertices[vIndex1] = position + Quaternion.Euler(0, 0, rotation) * new Vector3(-quadSize.x, +quadSize.y);
            vertices[vIndex2] = position + Quaternion.Euler(0, 0, rotation) * new Vector3(+quadSize.x, +quadSize.y);
            vertices[vIndex3] = position + Quaternion.Euler(0, 0, rotation) * new Vector3(+quadSize.x, -quadSize.y);
        }
        else
        {
            vertices[vIndex0] = position+Quaternion.Euler(0, 0, rotation - 180) * quadSize; //2   8 pos
            vertices[vIndex1] = position+Quaternion.Euler(0, 0, rotation - 270) * quadSize;
            vertices[vIndex2] = position+Quaternion.Euler(0, 0, rotation - 0) * quadSize;
            vertices[vIndex3] = position+Quaternion.Euler(0, 0, rotation - 90) * quadSize;
        }

        //Create triangles
        int tIndex=quadIndex*6;     //3 4

        triangles[tIndex + 0] = vIndex0;   //5
        triangles[tIndex + 1] = vIndex1;
        triangles[tIndex + 2] = vIndex2;

        triangles[tIndex + 3] = vIndex0;  //5
        triangles[tIndex + 4] = vIndex2;
        triangles[tIndex + 5] = vIndex3;


        mesh.vertices=vertices;
        mesh.uv=uv;
        mesh.triangles=triangles;

        GetComponent<MeshFilter>().mesh=mesh;

    }

}
