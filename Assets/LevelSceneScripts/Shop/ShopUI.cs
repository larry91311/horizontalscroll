﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopUI : MonoBehaviour
{
    
    public GameObject whitePrefab;
    public GameObject blackPrefab;
    public GameObject redPrefab;


    public Item white;
    public Item whiteSecond;
    public Item black;
    public Item blackSecond;
    public Item red;


    public GameObject Player;
    private Inventory playerInventory;


    bool once=false;
    bool firstClickWhite=false;
    bool firstClickBlack=false;
    bool firstClickRed=false;


    ItemWorld whiteItemWorld;

    

    // Start is called before the first frame update
    void Start()
    {
        playerInventory=Player.GetComponent<PlayerWhenInventory>().inventory;

        
        

        
    }

    private void OnEnable() {
        
        
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void BuyWhite()
    {
        if(firstClickWhite==false)
        {
            Transform transform=Instantiate(ItemAssets.Instance.pfItemWorld,new Vector3(0,0,100),Quaternion.identity);
            ItemWorld itemWorld=transform.GetComponent<ItemWorld>();
            
            itemWorld.SetItem(white);

            
            
            playerInventory.AddItem(itemWorld.GetItem());
            itemWorld.DestroySelf();


            firstClickWhite=true;
        }
        else
        {
            Transform transform=Instantiate(ItemAssets.Instance.pfItemWorld,new Vector3(0,0,100),Quaternion.identity);
            ItemWorld itemWorld=transform.GetComponent<ItemWorld>();
        
            itemWorld.SetItem(whiteSecond);

        
        
            playerInventory.AddItem(itemWorld.GetItem());
            itemWorld.DestroySelf();
        }
        

        

        

        
        
        

        
    }

    public void BuyBlack()
    {
        if(firstClickBlack==false)
        {
        Transform transform=Instantiate(ItemAssets.Instance.pfItemWorld,new Vector3(0,0,200),Quaternion.identity);
        ItemWorld itemWorld=transform.GetComponent<ItemWorld>();
        itemWorld.SetItem(black);
        
        playerInventory.AddItem(itemWorld.GetItem());

        // firstClickBlack=true;
        }
        // else
        // {
        //     Transform transform=Instantiate(ItemAssets.Instance.pfItemWorld,new Vector3(0,0,200),Quaternion.identity);
        //     ItemWorld itemWorld=transform.GetComponent<ItemWorld>();
        //     itemWorld.SetItem(blackSecond);
            
        //     playerInventory.AddItem(itemWorld.GetItem());
        // }
        
    }

    public void BuyRed()
    {
        
        Transform transform=Instantiate(ItemAssets.Instance.pfItemWorld,new Vector3(0,0,300),Quaternion.identity);
        ItemWorld itemWorld=transform.GetComponent<ItemWorld>();
        itemWorld.SetItem(red);

        playerInventory.AddItem(itemWorld.GetItem());
        
    }


    
}
