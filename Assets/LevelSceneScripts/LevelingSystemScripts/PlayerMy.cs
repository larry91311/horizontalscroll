﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerMy : MonoBehaviour
{
    private LevelSystemMy levelSystem;



    public void SetLevelSystem(LevelSystemMy levelSystem)
    {
        this.levelSystem=levelSystem;
        levelSystem.OnLevelChanged+=LevelSystem_OnLevelChanged;
    }

    private void LevelSystem_OnLevelChanged(object sender, EventArgs e) {
        // PlayVictoryAnim();
        // SpawnParticleEffect();
        // Flash(new Color(1, 1, 1, 1));

        SetHealthBarSize(1.0f+(1f + levelSystem.GetLevelNumber()) * .1f);
    }

    private void SetHealthBarSize(float healthBarSize) {
        transform.Find("PlayerCanvas").transform.Find("HealthBar").localScale = new Vector3(healthBarSize, 1, 1);
    }

    // Start is called before the first frame update
    
}
