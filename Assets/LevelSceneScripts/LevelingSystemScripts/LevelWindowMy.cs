﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LevelWindowMy : MonoBehaviour
{
    private Text levelText;
    private Image experienceBarImage;
    private LevelSystemMy levelSystem;
    
    private LevelSystemAnimatedMy levelSystemAnimated;

    private void Awake() {
        levelText=transform.Find("LevelText").GetComponent<Text>();
        experienceBarImage=transform.Find("ExperienceBar").transform.Find("Bar").GetComponent<Image>();
    }


    private void SetExperienceBarSize(float experienceNormalized)
    {
        
        experienceBarImage.fillAmount=experienceNormalized;
        
    }

    private void SetLevelNumber(int levelNumber)
    {
        levelText.text="Level "+(levelNumber+1);

    }

    public void SetLevelSystem(LevelSystemMy levelSystem) {
        this.levelSystem = levelSystem;
    }


    public void SetLevelSystemAnimated(LevelSystemAnimatedMy levelSystemAnimated) {
        this.levelSystemAnimated = levelSystemAnimated;
        SetLevelNumber(levelSystemAnimated.GetLevelNumber());
        SetExperienceBarSize(levelSystemAnimated.GetExperienceNormalized());
        
        levelSystemAnimated.OnExperienceChanged += LevelSystemAnimated_OnExperienceChanged;
        levelSystemAnimated.OnLevelChanged += LevelSystemAnimated_OnLevelChanged;
    }



    private void LevelSystemAnimated_OnLevelChanged(object sender, System.EventArgs e) {
        // Level changed, update text
        SetLevelNumber(levelSystemAnimated.GetLevelNumber());
    }

    private void LevelSystemAnimated_OnExperienceChanged(object sender, System.EventArgs e) {
        // Experience changed, update bar size
        SetExperienceBarSize(levelSystemAnimated.GetExperienceNormalized());
    }




    


    
}
