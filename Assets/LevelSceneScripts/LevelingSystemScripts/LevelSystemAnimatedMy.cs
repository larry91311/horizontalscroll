﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;
using System;
public class LevelSystemAnimatedMy 
{
    public event EventHandler OnExperienceChanged;
    public event EventHandler OnLevelChanged;


    private int level;
    private int experience;
    private int experienceToNextLevel;



    private bool isAnimating;

    private LevelSystemMy levelSystem;

    public void SetLevelSystem(LevelSystemMy levelSystem)
    {
        this.levelSystem=levelSystem;

        level = levelSystem.GetLevelNumber();
        experience = levelSystem.GetExperience();
        experienceToNextLevel=levelSystem.GetExperienceToNextLevel();


        levelSystem.OnExperienceChanged+=LevelSystem_OnExperienceChanged;
        levelSystem.OnLevelChanged+=LevelSystem_OnLevelChanged;
    }
    

    public LevelSystemAnimatedMy(LevelSystemMy levelSystem)
    {
        SetLevelSystem(levelSystem);
        FunctionUpdater.Create(()=>Update());
    }




    private void LevelSystem_OnLevelChanged(object sender, System.EventArgs e) {
        isAnimating = true;
    }

    private void LevelSystem_OnExperienceChanged(object sender, System.EventArgs e) {
        isAnimating = true;
    }






    private void Update()
    {
        if(isAnimating)
        {
            if(level<levelSystem.GetLevelNumber())
            {
                AddExperience();
            }
            else 
            {
                if(experience<levelSystem.GetExperience())
                {
                    AddExperience();
                }
                else
                {
                    isAnimating=false;
                }
            }
            
        }
    }



    private void AddExperience()
    {
        experience++;
        if(experience>=experienceToNextLevel)
        {
            level++;
            experience=0;
            if (OnLevelChanged != null) OnLevelChanged(this, EventArgs.Empty);
        }
        if (OnExperienceChanged != null) OnExperienceChanged(this, EventArgs.Empty);
    }


    public int GetLevelNumber() {
        return level;
    }


    public float GetExperienceNormalized() {
        
            return (float)experience / levelSystem.GetExperienceToNextLevel();
        
    }
        
    
}
