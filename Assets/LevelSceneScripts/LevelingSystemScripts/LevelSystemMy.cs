﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class LevelSystemMy 
{
    public event EventHandler OnExperienceChanged;
    public event EventHandler OnLevelChanged;
    
    private int level=0;
    private int experience=0;
    private int experienceToNextLevel=100;

    public void AddExperience(int amount)
    {
        experience+=amount;
        while(experience>=experienceToNextLevel)
        {
            level++;
            experience-=experienceToNextLevel;

            if (OnLevelChanged != null) OnLevelChanged(this, EventArgs.Empty);
        }
        if (OnExperienceChanged != null) OnExperienceChanged(this, EventArgs.Empty);
        



    }


    public int GetLevelNumber()
    {
        return level;
    }

    public float GetExperienceNormalized()
    {
        return (float)experience/experienceToNextLevel;
    }


    public int GetExperience() {
        return experience;
    }

    public int GetExperienceToNextLevel() {
        return experienceToNextLevel;
    }

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
