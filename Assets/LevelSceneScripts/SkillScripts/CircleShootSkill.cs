﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CircleShootSkill : MonoBehaviour
{
    public bool skillCircleShoot=false;
    public GameObject bullet;

    public Animator animator;

    public float bulletSpeed = 20f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.F))
        {
            skillCircleShoot=true;
        }
    }


    public void CircleShoot(bool CircleShoot)
    {
        animator.SetBool("Skill", true);
        StartCoroutine(Shooting());
        animator.SetBool("Skill", false);
    }


    IEnumerator Shooting()
    {
        int n=12;
        float angle=360;
        for(int i=0;i<n;i++)
        {
            
            GameObject go = Instantiate(bullet, transform.position,Quaternion.identity);
            Rigidbody2D goRig=go.GetComponent<Rigidbody2D>();
            goRig.velocity=new Vector2(Mathf.Cos(angle/180*Mathf.PI),Mathf.Sin(angle/180*Mathf.PI))*bulletSpeed;
            angle-=360/n*2;
            
            yield return null;
        }
        
    }

}
