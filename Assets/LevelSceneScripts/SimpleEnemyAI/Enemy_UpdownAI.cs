﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_UpdownAI : MonoBehaviour
{
    public enum State
    {
        Roam,
        Attack,
    }

    private State state;

    public AnimationCurve xCurve;
    public AnimationCurve yCurve;

    float xCurveValue;
    float yCurveValue;

    private Rigidbody2D enemyRig;  //本體Rig


    private Vector3 startingPosition;


    float timer = 0f;
    float nextCoro=0;

    // Start is called before the first frame update
    void Start()
    {
        startingPosition=transform.position;
        enemyRig=GetComponent<Rigidbody2D>();
        state=State.Roam;
    }

    // Update is called once per frame
    void Update()
    {
        timer+=Time.deltaTime;
        

        switch(state)
        {
            
            default:
            case State.Roam:
            {
                if(Time.time>nextCoro)
                {
                    StartCoroutine(Teleport(startingPosition));
                    nextCoro=Time.time+10f;
                }
                
                break;
            }
            case State.Attack:
            {
                
                break;
            }
            
                
            

        }



    }



    IEnumerator Teleport(Vector3 startingPosition)
	{
		float timer = 0f;

		while (timer < 10f)
		{
			timer += Time.deltaTime;
			float xCurveValue = xCurve.Evaluate(timer);
			float yCurveValue = yCurve.Evaluate(timer);
            transform.position=startingPosition+new Vector3(xCurveValue,yCurveValue,0);
			yield return 1f;
		}

		
	}
}
