﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BossEnemyAI : MonoBehaviour
{
    public enum State
    {
        Roam,
        Attack,
    }

    private State state;

    private Vector3 startingPosition;
    private Vector3 roamPosition;
    public Vector3 roamPosition1;
    public Vector3 roamPosition2;


    public GameObject player;
    [Header("Boss")]
    public GameObject Head;
    private Rigidbody2D headRigid;
    private Rigidbody2D bossRig;  //本體Rig

    private bool facingRight=true;   //boss方向

    private float nextTurnTimer=0f;  //turn timer
    private float nextTurnCD=1.0f;


    float horizontalDirection=1;
    [Header("Roam")]
    [Space]
    private float attackDistance=5.0f;
    public float headRotation=10.0f;
    Vector3 currentEulerAngles;


    [Header("Attack")]
    [Space]
    public GameObject bossbullet;
    float preAttackTime = 0f, attackSpeed = 0.1f, bulletSpeed = 20f;



    private void Awake() {
        bossRig = GetComponent<Rigidbody2D>();
        state=State.Roam;
    }

    // Start is called before the first frame update
    void Start()
    {
        startingPosition = transform.position;
        roamPosition=GetRoamingPosition();
        transform.DOMove(GetRoamingPosition(), 5.0f);

        
    }

    // Update is called once per frame
    void Update()
    {
        // currentEulerAngles += new Vector3(0, 0, 1) * Time.deltaTime * headRotation;
        // Head.transform.eulerAngles = currentEulerAngles;

        switch(state)
        {
            
            default:
            case State.Roam:
            {
                Roam();
                if(Vector3.Distance(transform.position,player.transform.position)<attackDistance)
                {
                    state=State.Attack;
                    
                }

                break;
            }
            case State.Attack:
            {
                StartCoroutine(Shooting());
                break;
            }
            
                
            

        }
        
    }

    // private void OnCollisionEnter2D(Collision2D other) {
    //     if(other.gameObject.tag=="Ground")
    //     {
    //         horizontalDirection*=-1;
    //         transform.DOMove(GetRoamingPosition(), 10.0f);

            
    //     }
    // }


    



    private Vector3 GetRoamingPosition() {            //水平移動
        
        return facingRight?roamPosition1:roamPosition2;
    }


    private void Roam()
    {   
        
        if(Vector3.Distance(transform.position,roamPosition1)<1.0f&&Time.time>nextTurnTimer)
        {
            facingRight=false;
            
            nextTurnTimer=Time.time+nextTurnCD;
        }

        if(Vector3.Distance(transform.position,roamPosition2)<1.0f&&Time.time>nextTurnTimer)
        {
            facingRight=true;

            nextTurnTimer=Time.time+nextTurnCD;
        }

        transform.DOMove(GetRoamingPosition(), 5.0f);
        
    }




    IEnumerator Shooting()
    {
        int n=40;
        float angle=360;
        for(int i=0;i<n;i++)
        {
            
            GameObject go = Instantiate(bossbullet, transform.position,Quaternion.identity);
            Rigidbody2D goRig=go.GetComponent<Rigidbody2D>();
            goRig.velocity=new Vector2(Mathf.Cos(angle/180*Mathf.PI),Mathf.Sin(angle/180*Mathf.PI))*bulletSpeed;
            angle-=360/n;
            
            yield return 0.1f;
        }

        state=State.Roam;
        
    }
}
