﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_PatrolAI : MonoBehaviour
{
    public enum State
    {
        Roam,
        Attack,
    }

    private State state;

    private Vector3 startingPosition;
    public Transform roamPosition1;
    public Transform roamPosition2;
    public Transform roamPosition3;
    public Transform roamPosition4;

    int path=1;
    private float speed=3.0f;

    public GameObject player;


    private Rigidbody2D enemyRig;  //本體Rig

    // Start is called before the first frame update
    void Start()
    {
        
        path=1;
        enemyRig=GetComponent<Rigidbody2D>();
        state=State.Roam;
        

        
    }

    // Update is called once per frame
    void Update()
    {
        
        switch(state)
        {
            
            default:
            case State.Roam:
            {
                if(Vector3.Distance(transform.position,roamPosition1.position)<0.1f)
                {
                    path=2;
                    
                }
                if(Vector3.Distance(transform.position,roamPosition2.position)<0.1f)
                {
                    path=3;
                    
                }
                if(Vector3.Distance(transform.position,roamPosition3.position)<0.1f)
                {
                    path=4;
                    
                }
                if(Vector3.Distance(transform.position,roamPosition4.position)<0.1f)
                {
                    path=1;
                    
                }
                GetNextPoint(path);
                break;
            }
            case State.Attack:
            {
                
                break;
            }
            
                
            

        }
    }


    private Vector3 GetRoamingPosition(int path) {   
           
        switch(path)
        {
            
            default:
            case 1: return roamPosition1.position; 
            case 2: return roamPosition2.position;
            case 3: return roamPosition3.position;
            case 4: return roamPosition4.position;
                
            

        }

        
        
    }

    private void GetNextPoint(int path)
    {
        Vector3 dir = GetRoamingPosition(path) - transform.position;
		transform.Translate(dir.normalized * speed * Time.deltaTime);
    }
}
