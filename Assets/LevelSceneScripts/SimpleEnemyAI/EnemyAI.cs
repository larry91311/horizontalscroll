﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;
using DG.Tweening;

public class EnemyAI : MonoBehaviour
{
    public enum State
    {
        Roam,
        Bigger,
        Attack,
    }
    private State state;

    private Vector3 startingPosition;
    private Vector3 roamPosition;
    
    float horizontalDirection=1;

    public GameObject player;
    public float biggerDistance=5.0f;
    private void Awake() {
        state=State.Roam;
    }

     
    // Start is called before the first frame update
    void Start()
    {
        startingPosition = transform.position;
        roamPosition=GetRoamingPosition();
        transform.DOMove(GetRoamingPosition(), 5.0f);
    }

    // Update is called once per frame
    void Update()
    {
        
        switch(state)
        {
            default:
            case State.Roam:
            {
                if(Vector3.Distance(transform.position,player.transform.position)<biggerDistance)
                {
                    state=State.Bigger;
                    // Vector3 EnemyScale=transform.localScale;
                    // EnemyScale*=2;
                    // transform.localScale=EnemyScale;
                }

                break;
            }
            case State.Bigger:
            {
                CircleCollider2D enemyCollider=GetComponent<CircleCollider2D>();
                enemyCollider.radius=0.5f;
                
                
                float timer=0f;
                timer+=Time.deltaTime;
                if(timer>2.0f)
                {
                    state=State.Roam;
                }
                break;
            }
            
                
            

        }
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if(other.gameObject.tag=="Ground")
        {
            horizontalDirection*=-1;
            transform.DOMove(GetRoamingPosition(), 7.0f);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag=="Player")
        {
            Rigidbody2D playerRig=other.GetComponent<Rigidbody2D>();
            playerRig.AddForce(new Vector2(other.transform.position.x-this.transform.position.x,other.transform.position.y-this.transform.position.y),ForceMode2D.Impulse);
            playerRig.AddForce(new Vector2(other.transform.position.x-this.transform.position.x,other.transform.position.y-this.transform.position.y),ForceMode2D.Impulse);
            playerRig.AddForce(new Vector2(other.transform.position.x-this.transform.position.x,other.transform.position.y-this.transform.position.y),ForceMode2D.Impulse);
            playerRig.AddForce(new Vector2(other.transform.position.x-this.transform.position.x,other.transform.position.y-this.transform.position.y),ForceMode2D.Impulse);
            playerRig.AddForce(new Vector2(other.transform.position.x-this.transform.position.x,other.transform.position.y-this.transform.position.y),ForceMode2D.Impulse);
        }
    }

    


    private Vector3 GetRoamingPosition() {            //水平移動
        
        return startingPosition + new Vector3(40,0,0) *horizontalDirection;
    }
}
