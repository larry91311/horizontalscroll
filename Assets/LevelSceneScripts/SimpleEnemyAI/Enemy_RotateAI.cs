﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_RotateAI : MonoBehaviour
{
    public enum State
    {
        Rotate,
        Attack,
    }


    private State state;

    private Rigidbody2D enemyRig;  //本體Rig


    Vector3 currentEulerAngles;
    float rotationSpeed = 45;



    public Transform rotateCenter;


    // Start is called before the first frame update
    void Start()
    {
        state=State.Rotate;
    }

    // Update is called once per frame
    void Update()
    {
        switch(state)
        {
            
            default:
            case State.Rotate:
            {
                currentEulerAngles += new Vector3(0, 0, 1) * Time.deltaTime * rotationSpeed;  //以旋轉軸來rotate
                rotateCenter.eulerAngles = currentEulerAngles;
                
                break;
            }
            case State.Attack:
            {
                
                break;
            }
            
                
            

        }
    }
}
