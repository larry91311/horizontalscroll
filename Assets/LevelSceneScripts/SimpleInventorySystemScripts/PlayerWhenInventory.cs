﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;
using V_AnimationSystem;

public class PlayerWhenInventory : MonoBehaviour
{
    public static PlayerWhenInventory Instance { get; private set; }
    
    public Inventory inventory;

    
    [SerializeField] private UI_Inventory uiInventory;
    private void Awake() {

        Instance = this;
        inventory=new Inventory(UseItem);
        uiInventory.SetInventory(inventory);
        uiInventory.SetPlayer(this);
        
        // ItemWorld.SpawnItemWorld(new Vector3(-2.1f,1),new Item{itemType=Item.ItemType.Circle,amount=1});
        // ItemWorld.SpawnItemWorld(new Vector3(-3f,1),new Item{itemType=Item.ItemType.Square,amount=1});
        // ItemWorld.SpawnItemWorld(new Vector3(-4f,1),new Item{itemType=Item.ItemType.Triangle,amount=1});
    }
    
    private void OnTriggerEnter2D(Collider2D collider) {
        ItemWorld itemWorld=collider.GetComponent<ItemWorld>();
        
        if(itemWorld!=null)
        {
            
            inventory.AddItem(itemWorld.GetItem());
            itemWorld.DestroySelf();
        }
    }


    public Vector3 GetPosition() {
        return transform.position;
    }



    private void UseItem(Item item) {
        switch (item.itemType) {
        case Item.ItemType.Circle:
            FlashGreen();
            inventory.RemoveItem(new Item { itemType = Item.ItemType.Circle, amount = 1 });
            break;
        case Item.ItemType.Square:
            FlashBlue();
            inventory.RemoveItem(new Item { itemType = Item.ItemType.Square, amount = 1 });
            break;
        }
    }



    public void FlashGreen() {
        print("4");
    }

    public void FlashRed() {
        print("5");
    }

    public void FlashBlue() {
        print("6");
    }
}
