﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;
using TMPro;
public class ItemWorld : MonoBehaviour
{
    



    private Item item;

    private SpriteRenderer spriteRenderer;

    private TextMeshPro textMeshPro;
    private void Awake() {
        spriteRenderer=GetComponent<SpriteRenderer>();
        textMeshPro=transform.GetChild(0).GetComponent<TextMeshPro>();
    }

    public void SetItem(Item item)
    {
        
        this.item=item;
        spriteRenderer.sprite=item.GetSprite();
        if (item.amount > 1) {
            textMeshPro.SetText(item.amount.ToString());
        } else {
            textMeshPro.SetText("");
        }
    }

    public Item GetItem()
    {
        return item;
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }

    

    public static ItemWorld SpawnItemWorld(Vector3 position, Item item) 
    {
        Transform transform=Instantiate(ItemAssets.Instance.pfItemWorld,position,Quaternion.identity);
        ItemWorld itemWorld=transform.GetComponent<ItemWorld>();
        itemWorld.SetItem(item);

        return itemWorld;


    }

    public static ItemWorld SpawnItemOuterSpace(Item item) 
    {
        Transform transform=Instantiate(ItemAssets.Instance.pfItemWorld,new Vector3(0,0,100),Quaternion.identity);
        ItemWorld itemWorld=transform.GetComponent<ItemWorld>();
        itemWorld.SetItem(item);

        return itemWorld;


    }



    public static ItemWorld DropItem(Vector3 dropPosition, Item item) {
        Vector3 randomDir = UtilsClass.GetRandomDir();
        ItemWorld itemWorld = SpawnItemWorld(dropPosition + randomDir * 8f, item);
        itemWorld.GetComponent<Rigidbody2D>().AddForce(randomDir * 40f, ForceMode2D.Impulse);
        return itemWorld;
    }




    public void ReSetItem(Item item)  //Try
    {
        
        
        this.item=item;
        
        item.amount=1;
        spriteRenderer.sprite=item.GetSprite();
        if (item.amount > 1) {
            textMeshPro.SetText(item.amount.ToString());
        } else {
            textMeshPro.SetText("");
        }
    }



    
}
