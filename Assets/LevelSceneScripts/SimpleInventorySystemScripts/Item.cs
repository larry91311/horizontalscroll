﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public class Item
{
    public enum ItemType
    {
        Circle,
        Square,
        Triangle,
        
    }


    public ItemType itemType;
    public int amount;


    public Sprite GetSprite()
    {
        switch(itemType)
        {
            default:
            case ItemType.Circle:        return ItemAssets.Instance.circleSprite;
            case ItemType.Square:        return ItemAssets.Instance.squareSprite;
            case ItemType.Triangle:      return ItemAssets.Instance.triangleSprite;
            
        }
    }

    public Color GetColor()
    {
        switch(itemType)
        {
            default:
            case ItemType.Circle:       return new Color(0,0,0);
            case ItemType.Square:       return new Color(0,1,0);
            case ItemType.Triangle:       return new Color(0,0,1);

        }
    }
    

    public bool IsStackable()   //可否儲存，像藥水可使用可多重儲存，刀劍不可多重儲存可使用
    {
        switch(itemType)
        {
            default:
            case ItemType.Circle:
            case ItemType.Square:
            return true;
            case ItemType.Triangle:
            return false;
        }
    }
}
