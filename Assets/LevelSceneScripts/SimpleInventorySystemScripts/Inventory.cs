﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Inventory 
{
    public event EventHandler OnItemListChanged;

    private Action<Item> useItemAction;


    public Inventory(Action<Item> useItemAction)
    {
        this.useItemAction = useItemAction;
        itemList=new List<Item>();
        
        // AddItem(new Item{itemType=Item.ItemType.Circle,amount=1});
        // AddItem(new Item{itemType=Item.ItemType.Square,amount=1});
        
    }
    private List<Item> itemList;          //物件存在這個LIST裡

    public List<Item> GetItemList()
    {
        return itemList;
    }
    

    public void AddItem(Item item)
    {
        if (item.IsStackable()) {                 //針對可儲存與不可儲存 以及   遇到已經有與還未有初始狀態情況
            bool itemAlreadyInInventory = false;
            foreach (Item inventoryItem in itemList) {
                if (inventoryItem.itemType == item.itemType) {
                    inventoryItem.amount += item.amount;
                    itemAlreadyInInventory = true;
                }
            }
            if (!itemAlreadyInInventory) {
                itemList.Add(item);
            }
        } 
        else {
            itemList.Add(item);
        }
        OnItemListChanged?.Invoke(this,EventArgs.Empty);
    }

    public void RemoveItem(Item item)
    {
        if (item.IsStackable()) {
            Item itemInInventory = null;
            foreach (Item inventoryItem in itemList) {
                if (inventoryItem.itemType == item.itemType) {
                    inventoryItem.amount -= item.amount;
                    itemInInventory = inventoryItem;
                }
            }
            if (itemInInventory != null && itemInInventory.amount <= 0) {
                itemList.Remove(itemInInventory);
            }
        } else {
            itemList.Remove(item);
        }
        OnItemListChanged?.Invoke(this, EventArgs.Empty);
    }

    public void UseItem(Item item) {
        useItemAction(item);
    }


}
