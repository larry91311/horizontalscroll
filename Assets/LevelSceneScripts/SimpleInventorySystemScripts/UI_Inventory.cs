﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CodeMonkey.Utils;
using TMPro;

public class UI_Inventory : MonoBehaviour
{
    private Inventory inventory;

    private PlayerWhenInventory player;

    public void SetPlayer(PlayerWhenInventory player)
    {
        this.player=player;
    }


    private Transform itemSlotContainer;
    private Transform itemSlotTemplate;

    private void RefreshInventoryItems()
    {
        foreach(Transform child in itemSlotContainer)    //創新的前刪掉舊的
        {
            if(child==itemSlotTemplate) continue;

            Destroy(child.gameObject);
        }

        // float x = 0f;
        // float y = 0f;             //用GridLayoutGroup取代
        // float itemSlotCellSize = 10f;
        foreach(Item item in inventory.GetItemList())
        {
            RectTransform itemSlotRectTransform=Instantiate(itemSlotTemplate,itemSlotContainer).GetComponent<RectTransform>();
            itemSlotRectTransform.gameObject.SetActive(true);

            itemSlotRectTransform.GetComponent<Button_UI>().ClickFunc = () => {             //左鍵右鍵 左鍵USE 右鍵DROP
                // Use item
                inventory.UseItem(item);
            };
            itemSlotRectTransform.GetComponent<Button_UI>().MouseRightClickFunc = () => {
                // Drop item
                Item duplicateItem = new Item { itemType = item.itemType, amount = item.amount };
                inventory.RemoveItem(item);
                ItemWorld.DropItem(player.GetPosition(), duplicateItem);
            };

            // itemSlotRectTransform.anchoredPosition=new Vector2(x*itemSlotCellSize,y*itemSlotCellSize);

            

            Image image=itemSlotRectTransform.Find("image").GetComponent<Image>();
            image.sprite=item.GetSprite();

            TextMeshProUGUI uiText=itemSlotRectTransform.Find("text").GetComponent<TextMeshProUGUI>();
            if(item.amount>1)
            {
                uiText.SetText(item.amount.ToString());

            }
            else
            {
                uiText.SetText("");
            }
            // x=x+2;

        }
    }
    public void SetInventory(Inventory inventory)
    {
        this.inventory=inventory;

        inventory.OnItemListChanged+=Inventory_OnItemListChanged;
        RefreshInventoryItems();
    }

    private void Inventory_OnItemListChanged(object sender, System.EventArgs e) {
        RefreshInventoryItems();
    }

    private void Awake() {
        itemSlotContainer=transform.Find("itemSlotContainerPanel");
        itemSlotTemplate=transform.Find("itemSlotContainerPanel").transform.Find("itemSlotTemplate");
    }
}
