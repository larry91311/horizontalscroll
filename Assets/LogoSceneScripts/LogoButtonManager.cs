﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class LogoButtonManager : MonoBehaviour
{
    public string levelToLoad = "LoadSceneName";

	public SceneFader sceneFader;

	public void Play ()
	{
		sceneFader.FadeTo("LevelSelect");
	}

	public void Quit ()
	{
		Debug.Log("Exciting...");
		Application.Quit();
	}



}
